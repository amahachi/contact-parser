package controllers

import (
	"contact-parser/internal/utils"
	"contact-parser/pkg/dto"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
)

type (
	ValidationHandler struct {
		log *logrus.Entry
	}
)

var (
	delimiters      = []string{".", ",", ";", "\n"}
	namePrefixes    = []string{"name", "client", "fio", "имя", "фио", "клиент", "Name", "Client", "Fio", "Имя", "Фио", "Клиент"}
	phonePrefixes   = []string{"телефон", "phone", "номер", "клиент", "client"}
	addressPrefixes = []string{"адрес", "куда", "адресс", "adres", "address", "addres", "Адрес", "Куда", "Адресс", "Adres", "Address", "Addres"}
	emailPrefixes   = []string{"мэйл", "мейл", "майл", "мэил", "меил", "маил", "email", "emial", "mail", "почта", "электронка", "электронная"}
)

func (h *ValidationHandler) Validate(ctx echo.Context) error {
	h.log.Debugf("handling validation")

	payload := new(dto.ParsePayload)
	err := ctx.Bind(payload)
	if err != nil {
		err = fmt.Errorf("error binding request to dto: %s", err)
		h.log.Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	h.log.Debugf("payload %+v", payload)

	contact, err := h.parseContactFromString(payload.String)
	if err != nil {
		err = fmt.Errorf("error parsing contact from string: %s", err)
		h.log.Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return ctx.JSON(http.StatusOK, contact)
}

func (h *ValidationHandler) parseContactFromString(input string) (*dto.Contact, error) {
	contact := &dto.Contact{}

	input = utils.TrimDelimiters(input)
	tokens := utils.SplitStringToTokens(input)
	h.log.Debugf("tokens: %+v", tokens)

	h.log.Debugf("parsing email")

	emailTokenIndex := -1
	for i, token := range tokens {
		for _, prefix := range emailPrefixes {
			if strings.HasPrefix(strings.ToLower(token), prefix+":") {
				emailTokenIndex = i
				break
			}
			if strings.HasPrefix(strings.ToLower(token), prefix) {
				emailTokenIndex = i
				break
			}

		}
		if emailTokenIndex > 0 {
			break
		}
	}

	emailRe := regexp.MustCompile(`[a-zA-Z0-9+._-]+@[a-zA-Z0-9+._-]+\.[a-zA-Z0-9_-]+`)
	contact.Email = emailRe.FindString(input)

	if len(contact.Email) != 0 {
		h.log.Debugf("found email %s", contact.Email)

		for _, prefix := range emailPrefixes {
			index := strings.Index(strings.ToLower(input), prefix+":")
			if index > -1 {
				input = input[:index] + input[index+len(prefix)+1:]
			}

			index = strings.Index(strings.ToLower(input), prefix)
			if index > -1 {
				input = input[:index] + input[index+len(prefix)+1:]
			}
		}

		input = strings.ReplaceAll(input, contact.Email+".", "")
		input = strings.ReplaceAll(input, contact.Email+",", "")
		input = strings.ReplaceAll(input, contact.Email+";", "")
		input = strings.ReplaceAll(input, contact.Email+"\n", "")
		input = strings.ReplaceAll(input, contact.Email, "")
	}

	if len(contact.Email) == 0 {
		h.log.Errorf("email not found")
	}

	input = utils.TrimDelimiters(input)
	tokens = utils.SplitStringToTokens(input)
	h.log.Debugf("tokens without email: %+v, input without email: %s", tokens, input)

	h.log.Debugf("parsing phone number")

	phoneTokenIndex := -1
	for i, token := range tokens {
		for _, prefix := range phonePrefixes {
			if strings.HasPrefix(strings.ToLower(token), prefix+":") {
				rawPhone := strings.TrimSpace(strings.TrimPrefix(strings.ToLower(token), prefix+":"))
				phone := ""
				for _, r := range []rune(rawPhone) {
					if strings.ContainsAny(string(r), "+0123456789") {
						if len(phone) == 0 {
							if string(r) == "7" || string(r) == "8" {
								phone = "+7"
								continue
							}
							if string(r) == "+" {
								phone += string(r)
								continue
							}
							phone += "+7" + string(r)
						} else if string(r) == "+" {
							h.log.Errorf("wrong phone token %s format", token)
						}
						phone += string(r)
					}
				}
				contact.Phone = phone
				phoneTokenIndex = i
				break
			}
			if strings.HasPrefix(strings.ToLower(token), prefix) {
				rawPhone := strings.TrimSpace(strings.TrimPrefix(strings.ToLower(token), prefix))
				phone := "+"
				for _, r := range []rune(rawPhone) {
					if strings.ContainsAny(string(r), "+0123456789") {
						if len(phone) == 0 {
							if string(r) == "7" || string(r) == "8" {
								phone = "+7"
								continue
							}
							if string(r) == "+" {
								phone += string(r)
								continue
							}
							phone += "+7" + string(r)
						} else if string(r) == "+" {
							h.log.Errorf("wrong phone token %s format", token)
						}
						phone += string(r)
					}
				}
				contact.Phone = phone
				phoneTokenIndex = i
				break
			}

		}
		if phoneTokenIndex > 0 {
			break
		}

	}
	if len(contact.Phone) != 0 {
		input = strings.ReplaceAll(input, tokens[phoneTokenIndex]+".", "")
		input = strings.ReplaceAll(input, tokens[phoneTokenIndex]+",", "")
		input = strings.ReplaceAll(input, tokens[phoneTokenIndex]+";", "")
		input = strings.ReplaceAll(input, tokens[phoneTokenIndex]+"\n", "")
		input = strings.ReplaceAll(input, tokens[phoneTokenIndex], "")
	} else {
		for i, t := range tokens {
			phone := ""
			for _, r := range []rune(t) {
				if !strings.ContainsAny(string(r), "0123456789()+- ") {
					h.log.Debugf("token %s does not contain phone number", t)
					goto nextToken
				}
				if strings.ContainsAny(string(r), "0123456789+") {
					if len(phone) == 0 {
						if string(r) == "7" || string(r) == "8" {
							phone = "+7"
							continue
						}
						if string(r) == "+" {
							phone += string(r)
							continue
						}
						phone += "+7" + string(r)
						continue
					} else if string(r) == "+" {
						h.log.Errorf("wrong phone token %s format", t)
					}
					phone += string(r)
				}
			}
			contact.Phone = phone
			phoneTokenIndex = i

			input = strings.ReplaceAll(input, t+".", "")
			input = strings.ReplaceAll(input, t+",", "")
			input = strings.ReplaceAll(input, t+";", "")
			input = strings.ReplaceAll(input, t+"\n", "")
			input = strings.ReplaceAll(input, t, "")

			break
		nextToken:
			continue
		}
	}

	if len(contact.Phone) == 0 {
		h.log.Errorf("phone not found")
	}

	input = utils.TrimDelimiters(input)
	tokens = utils.SplitStringToTokens(input)
	h.log.Debugf("tokens without phone and email: %+v, input:%s", tokens, input)

	nameTokenIndex := -1
	for i, token := range tokens {
		for _, prefix := range namePrefixes {
			if strings.HasPrefix(token, prefix+":") {
				contact.Client = strings.TrimSpace(strings.TrimPrefix(token, prefix+":"))
				nameTokenIndex = i
				break
			}
			if strings.HasPrefix(token, prefix) {
				contact.Client = strings.TrimSpace(strings.TrimPrefix(token, prefix))
				nameTokenIndex = i
				break
			}
		}
	}

	if nameTokenIndex > -1 {
		input = strings.ReplaceAll(input, tokens[nameTokenIndex]+".", "")
		input = strings.ReplaceAll(input, tokens[nameTokenIndex]+",", "")
		input = strings.ReplaceAll(input, tokens[nameTokenIndex]+";", "")
		input = strings.ReplaceAll(input, tokens[nameTokenIndex]+"\n", "")
		input = strings.ReplaceAll(input, tokens[nameTokenIndex], "")
	} else {
		h.log.Debugf("phone token index %d", phoneTokenIndex)
		nameTokenIndex = -1
		if phoneTokenIndex == 0 {
			nameTokenIndex = len(tokens) - 1
		}
		if phoneTokenIndex == 1 {
			nameTokenIndex = 0
		}
		h.log.Debugf("name token index %d", nameTokenIndex)
		if nameTokenIndex > -1 {
			contact.Client = utils.TrimDelimiters(tokens[nameTokenIndex])
			input = strings.ReplaceAll(input, tokens[nameTokenIndex]+".", "")
			input = strings.ReplaceAll(input, tokens[nameTokenIndex]+",", "")
			input = strings.ReplaceAll(input, tokens[nameTokenIndex]+";", "")
			input = strings.ReplaceAll(input, tokens[nameTokenIndex]+"\n", "")
			input = strings.ReplaceAll(input, tokens[nameTokenIndex], "")
		}
	}

	input = utils.TrimDelimiters(input)
	tokens = utils.SplitStringToTokens(input)
	h.log.Debugf("tokens without phone, email and name: %+v, input: %s", tokens, input)

	addresWithPrefix := false
	for _, prefix := range addressPrefixes {
		index := strings.Index(input, prefix+":")
		if index > -1 {
			addresWithPrefix = true
			input = input[index:]
			contact.Address = strings.TrimSpace(strings.TrimPrefix(strings.TrimSpace(input), prefix+":"))
			break
		}

		index = strings.Index(input, prefix)
		if index > -1 {
			addresWithPrefix = true
			input = input[index:]
			contact.Address = strings.TrimSpace(strings.TrimPrefix(strings.TrimSpace(input), prefix))
			break
		}
	}

	if !addresWithPrefix {
		contact.Address = utils.TrimDelimiters(input)
	}

	return contact, nil
}

func NewValidationHanlder(
	log *logrus.Entry,
) *ValidationHandler {
	return &ValidationHandler{
		log,
	}
}
