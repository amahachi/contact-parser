package controllers

import (
	"contact-parser/pkg/dto"
	"testing"

	"github.com/sirupsen/logrus"
)

type fixture struct {
	CaseNumber      int
	String          string
	ExpectedContact dto.Contact
}

func TestParseContactFromString(t *testing.T) {
	svc := NewValidationHanlder(logrus.New().WithField("method", "TestValidation"))

	t.Logf("running %d test cases", len(fixtures))
	for _, f := range fixtures {
		actualContact, err := svc.parseContactFromString(f.String)
		if err != nil {
			t.Fatalf("error parsing contact string for case %d: %s", f.CaseNumber, err)
		}
		if actualContact.Client != f.ExpectedContact.Client {
			t.Fatalf("failed to parse client for case %d: expected %s, got %s", f.CaseNumber, f.ExpectedContact.Client, actualContact.Client)
		}
		if actualContact.Phone != f.ExpectedContact.Phone {
			t.Fatalf("failed to parse phone for case %d: expected %s, got %s", f.CaseNumber, f.ExpectedContact.Phone, actualContact.Phone)
		}
		if actualContact.Email != f.ExpectedContact.Email {
			t.Fatalf("failed to parse email for case %d: expected %s, got %s", f.CaseNumber, f.ExpectedContact.Email, actualContact.Email)
		}
		if actualContact.Address != f.ExpectedContact.Address {
			t.Fatalf("failed to parse address for case %d: expected %s, got %s", f.CaseNumber, f.ExpectedContact.Address, actualContact.Address)
		}
	}
}

var fixtures = []fixture{
	{
		CaseNumber: 1,
		String:     "Name: Анна, Phone: +7 (915) 255-11-65, Email: INFO@REKLAINER.RU, Address: ул.Полбина, 14, кв 346",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина, 14, кв 346",
		},
	},
	{
		CaseNumber: 2,
		String:     "Имя: Анна. Телефон: +7 (915) 255-11-65. Майл: INFO@REKLAINER.RU, Адрес: .",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "",
		},
	},
	{
		CaseNumber: 3,
		String:     "имя: Анна\nтелефон: +7 (915) 255-11-65\nмайл: INFO@REKLAINER.RU\nАдрес: ",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "",
		},
	},
	{
		CaseNumber: 4,
		String:     "Имя: Анна;Телефон: +7 (915) 255-11-65\nМайл: INFO@REKLAINER.RU\nАдрес: ",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "",
		},
	},
	{
		CaseNumber: 5,
		String:     "Анна, +7 (915) 255-11-65, INFO@REKLAINER.RU, Москва, ул.Полбина, д.14",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "Москва, ул.Полбина, д.14",
		},
	},
	{
		CaseNumber: 6,
		String:     "Анна, +7 (915) 255-11-65, INFO@REKLAINER.RU, ул.Полбина д.14",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 7,
		String:     "+7 (915) 255-11-65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 8,
		String:     "+79152551165, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 9,
		String:     "+7 915 255 11 65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 10,
		String:     "+7 915 255-11-65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 11,
		String:     "915 255-11-65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 12,
		String:     "915 255 11 65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU,",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 13,
		String:     "8915 255 11 65, ул.Полбина д.14, Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 14,
		String:     "8 915 255 11 65, ул.Полбина д.14, Анна Васильевна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Анна Васильевна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 15,
		String:     "8 915 255 11 65, ул.Полбина д.14, ООО Стройтех, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "ООО Стройтех",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 16,
		String:     "8 915 255 11 65, ул.Полбина д.14, Стройтех, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Стройтех",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 17,
		String:     "8 915 255 11 65, ул.Полбина д.14, Анна Васильевна Козлова, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Анна Васильевна Козлова",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 18,
		String:     "8 915 255 11 65, ул.Полбина д.14, Козлова Анна Васильевна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна Васильевна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 19,
		String:     "8 915 255 11 65, ул.Полбина д.14, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д.14",
		},
	},
	{
		CaseNumber: 20,
		String:     "8 915 255 11 65, Полбина 14, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "Полбина 14",
		},
	},
	{
		CaseNumber: 21,
		String:     "8 915 255 11 65, Полбина 14/1, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "Полбина 14/1",
		},
	},
	{
		CaseNumber: 22,
		String:     "8 915 255 11 65, Полбина 14 к1, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "Полбина 14 к1",
		},
	},
	{
		CaseNumber: 23,
		String:     "8 915 255 11 65, ул.Полбина 14, к1 кв80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина 14, к1 кв80",
		},
	},
	{
		CaseNumber: 24,
		String:     "8 915 255 11 65, ул.Полбина д14 к1 кв80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д14 к1 кв80",
		},
	},
	{
		CaseNumber: 25,
		String:     "8 915 255 11 65, ул.Полбина д 14 к 1 кв 80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д 14 к 1 кв 80",
		},
	},
	{
		CaseNumber: 26,
		String:     "8 915 255 11 65, ул.Полбина д 14 а к 1 кв 80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д 14 а к 1 кв 80",
		},
	},
	{
		CaseNumber: 27,
		String:     "8 915 255 11 65, ул.Полбина д14а к 1 кв 80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д14а к 1 кв 80",
		},
	},
	{
		CaseNumber: 28,
		String:     "8 915 255 11 65, ул.Полбина д14 а к 1 кв 80, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д14 а к 1 кв 80",
		},
	},
	{
		CaseNumber: 29,
		String:     "8 915 255 11 65, ул.Полбина д14а к 1 80 кв 5 подъед 4 этаж, Козлова Анна, INFO@REKLAINER.RU",
		ExpectedContact: dto.Contact{
			Client:  "Козлова Анна",
			Phone:   "+79152551165",
			Email:   "INFO@REKLAINER.RU",
			Address: "ул.Полбина д14а к 1 80 кв 5 подъед 4 этаж",
		},
	},
}
