package internal

import (
	"contact-parser/internal/controllers"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type APIService struct {
	log               *logrus.Entry
	validationHandler *controllers.ValidationHandler
}

func (svc *APIService) RegisterRoutes(e *echo.Echo) {
	api := e.Group("")

	api.POST("/parse-contact", svc.validationHandler.Validate)
}

func (svc *APIService) Start() {
	e := echo.New()

	svc.RegisterRoutes(e)

	svc.log.Info("Starting HTTP server")
	listenAddr := viper.GetString("bind.address") + ":" + viper.GetString("bind.port")
	svc.log.Fatal(e.Start(listenAddr))
}

func NewAPIService(
	log *logrus.Entry,
) (*APIService, error) {
	return &APIService{
		log:               log,
		validationHandler: controllers.NewValidationHanlder(log),
	}, nil
}
