package utils

import "strings"

var (
	delimiters = []string{".", ",", ";", "\n"}
)

func TrimDelimiters(input string) string {
	input = strings.TrimSpace(input)
	for _, d := range delimiters {
		input = strings.TrimSuffix(input, d)
	}
	for _, d := range delimiters {
		input = strings.TrimPrefix(input, d)
	}
	return input
}

func SplitStringToTokens(input string) []string {
	input = strings.TrimSpace(input)

	dotSeparetedTokens := strings.Split(input, ".")

	commaSeparetedTokens := []string{}
	for _, token := range dotSeparetedTokens {
		token = strings.TrimSpace(token)
		if len(token) == 0 {
			continue
		}
		commaSeparetedTokens = append(commaSeparetedTokens, strings.Split(token, ",")...)
	}

	semicolonSeparatedTokens := []string{}
	for _, token := range commaSeparetedTokens {
		token = strings.TrimSpace(token)
		if len(token) == 0 {
			continue
		}
		semicolonSeparatedTokens = append(semicolonSeparatedTokens, strings.Split(token, ";")...)
	}

	carriageReturnSeparetedTokens := []string{}
	for _, token := range semicolonSeparatedTokens {
		token = strings.TrimSpace(token)
		if len(token) == 0 {
			continue
		}
		carriageReturnSeparetedTokens = append(carriageReturnSeparetedTokens, strings.Split(token, "\n")...)
	}

	return carriageReturnSeparetedTokens
}
