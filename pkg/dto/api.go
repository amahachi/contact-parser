package dto

type ParsePayload struct {
	String string `json:"string"`
}

type Contact struct {
	Client  string `json:"client"`
	Phone   string `json:"phone"`
	Email   string `json:"email"`
	Address string `json:"address"`
}
