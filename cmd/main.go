package main

import (
	"fmt"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"contact-parser/internal"
)

func main() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}

	viper.SetDefault("bind.address", "0.0.0.0")
	viper.SetDefault("bind.port", "8080")

	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: time.RFC3339,
	})
	switch viper.GetString("logging.level") {
	case "warning":
		logger.SetLevel(logrus.WarnLevel)
	case "notice":
		logger.SetLevel(logrus.InfoLevel)
	case "debug":
		logger.SetLevel(logrus.DebugLevel)
	default:
		logger.SetLevel(logrus.InfoLevel)
	}
	logger.Infof("Current log level: %s", logger.Level.String())

	env := viper.GetString("version")
	logger.Infof("version: %s", env)
	svc, err := internal.NewAPIService(logger.WithField("app", "contact-parser"))
	if err == nil {
		svc.Start()
	}
}
